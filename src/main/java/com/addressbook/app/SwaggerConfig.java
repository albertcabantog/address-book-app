package com.addressbook.app;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@Configuration
public class SwaggerConfig {                                    
    @Bean
    public Docket api() { 
        return new Docket(DocumentationType.SWAGGER_2)  
          .select()                                  
          .apis(RequestHandlerSelectors.basePackage("com.addressbook.app"))              
          .paths(PathSelectors.any())
          .build().apiInfo(metaData());                                  
    }
    
	private ApiInfo metaData() {
		Contact contact = new Contact("Albert Cabantog", "https://bitbucket.org/albertcabantog/address-book-app", "albertcabantog@gmail.com");
		return new ApiInfoBuilder()
				.title("Address Book App")
				.contact(contact)
				.description("RESTful Web service application that manages address book")
				.version("Version 1.0")
				.build();
    }
}
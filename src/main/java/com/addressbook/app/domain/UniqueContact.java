package com.addressbook.app.domain;

import lombok.Data;

@Data
public class UniqueContact {

	private String name;
	
	public UniqueContact() {
		
	}
	
	public UniqueContact(String name) {
		this.name = name;
	}
}

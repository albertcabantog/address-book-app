package com.addressbook.app;

import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@ComponentScan({ "com.addressbook.app" })
@EntityScan("com.addressbook.app.domain")
@EnableJpaRepositories("com.addressbook.app.dao")
@EnableConfigurationProperties
@SpringBootApplication
public class MainApplication {

	private static final Logger LOG = Logger.getLogger(MainApplication.class);

	public static void main(String[] args) {
		LOG.info("Start main application");
		SpringApplication.run(MainApplication.class, args);
	}

}

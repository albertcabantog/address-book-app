package com.addressbook.app.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.addressbook.app.domain.Contact;
import com.addressbook.app.domain.ContactNumber;

public interface ContactNumberDao extends JpaRepository<ContactNumber, Long> {

	@Query("select cn from ContactNumber cn where cn.contact = ?1 and cn.phoneNumber = ?2")
	ContactNumber findByPhoneNumber(Contact contact, String phoneNumber);
}

package com.addressbook.app.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.addressbook.app.domain.AddressBook;
import com.addressbook.app.domain.Contact;
import com.addressbook.app.domain.UniqueContact;

public interface ContactDao extends JpaRepository<Contact, Long> {
	
	@Query("select c from Contact c where c.addressBook = ?1 and name = ?2")
	Contact findByName(AddressBook addressBook, String name);

	@Query("select new com.addressbook.app.domain.UniqueContact(c.name) from Contact c group by c.name")
	List<UniqueContact> getDistinctContacts();
}

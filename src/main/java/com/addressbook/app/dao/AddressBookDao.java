package com.addressbook.app.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.addressbook.app.domain.AddressBook;

public interface AddressBookDao extends JpaRepository<AddressBook, Long> {
	
	@Query("select a from AddressBook a where a.name = ?1")
	AddressBook findByName(String name);
	
}

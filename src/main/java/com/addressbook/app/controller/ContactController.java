package com.addressbook.app.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.addressbook.app.dao.AddressBookDao;
import com.addressbook.app.dao.ContactDao;
import com.addressbook.app.dao.ContactNumberDao;
import com.addressbook.app.domain.AddressBook;
import com.addressbook.app.domain.Contact;
import com.addressbook.app.domain.ContactNumber;
import com.addressbook.app.domain.UniqueContact;
import com.addressbook.app.util.SystemUtils;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

@RestController
public class ContactController {

	private static final String HEADER_ERROR_MESSAGE = "error-message";

	private static final Logger LOG = Logger.getLogger(ContactController.class);

	@Autowired
	private AddressBookDao addressBookDao;

	@Autowired
	private ContactDao contactDao;

	@Autowired
	private ContactNumberDao contactNumberDao;

	@ApiOperation(value = "Creates new contact to a new address book or to a selected addressbook", 
			response = ResponseEntity.class)
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Success - new contact created"),
			@ApiResponse(code = 400, message = "Unable to create a new address book for a selected address book, "
					+ "Please select an address book, Invalid address book Id, New address book name is required"
					+ "Unable to use new addressbook name when using existing addressbook, Contact name is mandatory"
					+ "At least one phone number is required, Address book already exist, Name already exist") })
	@RequestMapping(value = "/api/createNewContact", method = RequestMethod.PUT)
	public ResponseEntity<String> createNewContact(
			@ApiParam(value = "selected addressbook Id to be used for the new contact")
			@RequestParam(required = false) String addressBookId,
			@ApiParam(value = "Name of the new contact to be created", required = true)
			@RequestParam String contactName, 
			@ApiParam(value = "Phone numbers to be associated to the contact person, "
					+ "supports multiple entries by using comma between values (e.g. 0477777777,0488888888)", required = true)
			@RequestParam String contactNumbers,
			@ApiParam(value = "Flag to indicate if a new address book is to be created for this contact.  Use 'y' to create a new addressbook.")
			@RequestParam(defaultValue = "n") String newAddressBookFlag,
			@ApiParam(value = "Name of the new addressbook")
			@RequestParam(required = false) String addressbookName) {
		LOG.info("Creating new contact");

		boolean isNewAddressBook = StringUtils.equalsIgnoreCase("y", newAddressBookFlag)
				&& StringUtils.isBlank(addressBookId);
		if (StringUtils.isNotBlank(addressBookId) && StringUtils.equalsIgnoreCase("y", newAddressBookFlag)) {
			return new ResponseEntity<String>("Unable to create a new address book for a selected address book",
					HttpStatus.BAD_REQUEST);
		} else if (StringUtils.equalsIgnoreCase("y", newAddressBookFlag) == false
				&& StringUtils.isBlank(addressBookId)) {
			return new ResponseEntity<String>("Please select an address book", HttpStatus.BAD_REQUEST);
		} else if (StringUtils.equalsIgnoreCase("y", newAddressBookFlag) == false
				&& StringUtils.isNumeric(addressBookId) == false) {
			return new ResponseEntity<String>("Invalid address book Id", HttpStatus.BAD_REQUEST);
		} else if (isNewAddressBook && StringUtils.isBlank(addressbookName)) {
			return new ResponseEntity<String>("New address book name is required", HttpStatus.BAD_REQUEST);
		} else if (isNewAddressBook == false && StringUtils.isNotBlank(addressbookName)) {
			return new ResponseEntity<String>("Unable to use new addressbook name when using existing addressbook", HttpStatus.BAD_REQUEST);
		}

		if (StringUtils.isBlank(contactName)) {
			return new ResponseEntity<String>("Contact name is mandatory", HttpStatus.BAD_REQUEST);
		}

		if (StringUtils.isBlank(contactNumbers)) {
			return new ResponseEntity<String>("At least one phone number is required", HttpStatus.BAD_REQUEST);
		}
		
		List<String> numbers = new ArrayList<>();
		Arrays.asList(StringUtils.splitByWholeSeparator(contactNumbers, ",")).stream().forEach(phoneNumber -> {
			numbers.add(phoneNumber.trim());
		});
		if (SystemUtils.hasDuplicatePhoneNumbers(numbers)) {
			return new ResponseEntity<String>("Duplicate phone numbers are not allowed", HttpStatus.BAD_REQUEST);
		}

		AddressBook addressBook;
		if (isNewAddressBook) {
			addressbookName = addressbookName.trim();
			addressBook = addressBookDao.findByName(addressbookName);
			if (addressBook != null) {
				return new ResponseEntity<String>("Address book already exist", HttpStatus.BAD_REQUEST);
			}

			addressBook = new AddressBook();
			addressBook.setName(addressbookName);
			addressBookDao.save(addressBook);
		} else {
			addressBook = addressBookDao.findOne(Long.valueOf(addressBookId));
		}
		
		if (isNewAddressBook == false) {
			Contact contact = contactDao.findByName(addressBook, contactName.trim());
			if (contact != null) {
				return new ResponseEntity<String>("Name already exist", HttpStatus.BAD_REQUEST);
			}
		}

		Contact contact = new Contact();
		contact.setAddressBook(addressBook);
		contact.setName(contactName.trim());

		Contact savedContact = contactDao.save(contact);

		List<ContactNumber> phoneNumberList = new ArrayList<>();
		numbers.stream().forEach(number -> {
			ContactNumber cn = new ContactNumber();
			cn.setContact(savedContact);
			cn.setPhoneNumber(number.trim());
			phoneNumberList.add(cn);
		});

		savedContact.setContactNumbers(phoneNumberList);

		contactDao.save(savedContact);

		return new ResponseEntity<String>("Success - new contact created", HttpStatus.OK);
	}

	@ApiOperation(value = "Returns the details of the contact including phone numbers", 
			response = ResponseEntity.class)
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Prints the contact details"),
			@ApiResponse(code = 400, 
					responseHeaders = {
							@ResponseHeader(name = HEADER_ERROR_MESSAGE, 
									responseContainer = "Contact not selected, Contact Id is invalid, Contact not found")
						}, message = "")})
	@RequestMapping(value = "/api/getContact", method = RequestMethod.GET)
	public ResponseEntity<Contact> getContact(
			@ApiParam(value = "Contact Id to be retrieved", required = true)
			@RequestParam String contactId) {
		
		if (StringUtils.isBlank(contactId)) {
			MultiValueMap<String, String> header = new LinkedMultiValueMap<>();
			header.set(HEADER_ERROR_MESSAGE, "Contact not selected");
			return new ResponseEntity<Contact>(header, HttpStatus.BAD_REQUEST);
		} else if (StringUtils.isNumeric(contactId) == false) {
			MultiValueMap<String, String> header = new LinkedMultiValueMap<>();
			header.set(HEADER_ERROR_MESSAGE, "Contact Id is invalid");
			return new ResponseEntity<Contact>(header, HttpStatus.BAD_REQUEST);
		}

		Contact contact = contactDao.findOne(Long.valueOf(contactId));
		if (contact == null) {
			MultiValueMap<String, String> header = new LinkedMultiValueMap<>();
			header.set(HEADER_ERROR_MESSAGE, "Contact not found");
			return new ResponseEntity<Contact>(header, HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<Contact>(contact, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Returns the unique set of contact names across all addressbook", response = ResponseEntity.class)
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Prints the addressbook found")})
	@RequestMapping(value = "/api/getDistinctContacts", method = RequestMethod.GET)
	public ResponseEntity<List<UniqueContact>> getDistinctContacts() {

		List<UniqueContact> contacts = contactDao.getDistinctContacts();
		if (CollectionUtils.isEmpty(contacts)) {
			MultiValueMap<String, String> header = new LinkedMultiValueMap<>();
			header.set(HEADER_ERROR_MESSAGE, "Contact not found");
			return new ResponseEntity<List<UniqueContact>>(header, HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<List<UniqueContact>>(contacts, HttpStatus.OK);
	}

	@ApiOperation(value = "Updates the name of the selected contact", response = ResponseEntity.class)
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Success - contact name updated"),
			@ApiResponse(code = 400, message = "Contact not selected, Contact Id is invalid, New name is required,"
					+ "Contact not found, Contact name already used")})
	@RequestMapping(value = "/api/updateContactName", method = RequestMethod.POST)
	public ResponseEntity<String> updateContactName(
			@ApiParam(value = "Selected contact Id to be updated", required = true)
			@RequestParam String contactId, 
			@ApiParam(value = "Name to be used for this update", required = true)
			@RequestParam String contactName) {

		if (StringUtils.isBlank(contactId)) {
			return new ResponseEntity<String>("Contact not selected", HttpStatus.BAD_REQUEST);
		} else if (StringUtils.isNumeric(contactId) == false) {
			return new ResponseEntity<String>("Contact Id is invalid", HttpStatus.BAD_REQUEST);
		} else if (StringUtils.isBlank(contactName)) {
			return new ResponseEntity<String>("New name is required", HttpStatus.BAD_REQUEST);
		}

		Contact contact = contactDao.findOne(Long.valueOf(contactId));
		if (contact == null) {
			return new ResponseEntity<String>("Contact not found", HttpStatus.NOT_FOUND);
		}
		
		AddressBook addressBook = contact.getAddressBook();
		if (contactDao.findByName(addressBook, contactName) != null) {
			return new ResponseEntity<String>("Contact name already used", HttpStatus.BAD_REQUEST);
		}

		contact.setName(contactName.trim());
		contactDao.save(contact);

		return new ResponseEntity<String>("Success - contact name updated", HttpStatus.OK);
	}
	
	@ApiOperation(value = "Adds a new phone number to the selected contact", response = ResponseEntity.class)
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Success - contact number added"),
			@ApiResponse(code = 400, message = "Contact not selected, Contact Id is invalid, New contact number is required,"
					+ "Contact not found, Phone number already used")})
	@RequestMapping(value = "/api/addContactNumber", method = RequestMethod.PUT)
	public ResponseEntity<String> addContactNumber(
			@ApiParam(value = "Selected contact Id to be used to add the phone number", required = true)
			@RequestParam String contactId,
			@ApiParam(value = "New contact number to be added", required = true)
			@RequestParam String newContactNumber) {

		if (StringUtils.isBlank(contactId)) {
			return new ResponseEntity<String>("Contact not selected", HttpStatus.BAD_REQUEST);
		} else if (StringUtils.isNumeric(contactId) == false) {
			return new ResponseEntity<String>("Contact Id is invalid", HttpStatus.BAD_REQUEST);
		} else if (StringUtils.isBlank(newContactNumber)) {
			return new ResponseEntity<String>("New contact number is required", HttpStatus.BAD_REQUEST);
		}
		
		Contact contact = contactDao.findOne(Long.valueOf(contactId));
		if (contact == null) {
			return new ResponseEntity<String>("Contact not found", HttpStatus.NOT_FOUND);
		}

		ContactNumber phoneNumber = contactNumberDao.findByPhoneNumber(contact, newContactNumber);
		if (phoneNumber != null) {
			return new ResponseEntity<String>("Phone number already used", HttpStatus.BAD_REQUEST);
		}
		
		phoneNumber = new ContactNumber();
		phoneNumber.setContact(contact);
		phoneNumber.setPhoneNumber(newContactNumber.trim());
		
		contactNumberDao.save(phoneNumber);

		return new ResponseEntity<String>("Success - contact number added", HttpStatus.OK);
	}

	@ApiOperation(value = "Updates the phone number of the selected contact number", response = ResponseEntity.class)
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Success - contact number added"),
			@ApiResponse(code = 400, message = "Contact number not selected, Contact number Id is invalid, New contact number is required,"
					+ "Contact number not found, Phone number already used")})
	@RequestMapping(value = "/api/updateContactNumber", method = RequestMethod.POST)
	public ResponseEntity<String> updateContactNumber(
			@ApiParam(value = "Selected contact number Id to be updated", required = true)
			@RequestParam String contactNumberId,
			@ApiParam(value = "New phone number to be used", required = true)
			@RequestParam String newContactNumber) {

		if (StringUtils.isBlank(contactNumberId)) {
			return new ResponseEntity<String>("Contact number not selected", HttpStatus.BAD_REQUEST);
		} else if (StringUtils.isNumeric(contactNumberId) == false) {
			return new ResponseEntity<String>("Contact number Id is invalid", HttpStatus.BAD_REQUEST);
		} else if (StringUtils.isBlank(newContactNumber)) {
			return new ResponseEntity<String>("New contact number is required", HttpStatus.BAD_REQUEST);
		}

		newContactNumber = newContactNumber.trim();
		ContactNumber phoneNumber = contactNumberDao.findOne(Long.valueOf(contactNumberId));
		if (phoneNumber == null) {
			return new ResponseEntity<String>("Contact number not found", HttpStatus.NOT_FOUND);
		}
		
		Contact contact = phoneNumber.getContact();
		if (contactNumberDao.findByPhoneNumber(contact, newContactNumber) != null) {
			return new ResponseEntity<String>("Phone number already used", HttpStatus.BAD_REQUEST);
		}

		phoneNumber.setPhoneNumber(newContactNumber.trim());
		contactNumberDao.save(phoneNumber);

		return new ResponseEntity<String>("Success - contact number updated", HttpStatus.OK);
	}
	
	@ApiOperation(value = "Deletes a contact along with its phone numbers", response = ResponseEntity.class)
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Success - contact deleted"),
			@ApiResponse(code = 400, message = "Contact not selected, Contact Id is invalid, "
					+ "Contact not found")})
	@RequestMapping(value = "/api/deleteContact", method = RequestMethod.DELETE)
	public ResponseEntity<String> deleteContact(
			@ApiParam(value = "Selected contact Id to be deleted", required = true)
			@RequestParam String contactId) {

		if (StringUtils.isBlank(contactId)) {
			return new ResponseEntity<String>("Contact not selected", HttpStatus.BAD_REQUEST);
		} else if (StringUtils.isNumeric(contactId) == false) {
			return new ResponseEntity<String>("Contact Id is invalid", HttpStatus.BAD_REQUEST);
		}

		Contact contact = contactDao.findOne(Long.valueOf(contactId));
		if (contact == null) {
			return new ResponseEntity<String>("Contact not found", HttpStatus.NOT_FOUND);
		}

		contactDao.delete(contact);

		return new ResponseEntity<String>("Success - contact deleted", HttpStatus.OK);
	}
	
	@ApiOperation(value = "Deletes a phone numbers from a contact", response = ResponseEntity.class)
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Success - contact number deleted"),
			@ApiResponse(code = 400, message = "Contact number not selected, Contact number Id is invalid, "
					+ "Contact number not found")})
	@RequestMapping(value = "/api/deleteContactNumber", method = RequestMethod.DELETE)
	public ResponseEntity<String> deleteContactNumber(
			@ApiParam(value = "Selected contact number Id to be deleted", required = true)
			@RequestParam String contactNumberId) {

		if (StringUtils.isBlank(contactNumberId)) {
			return new ResponseEntity<String>("Contact number not selected", HttpStatus.BAD_REQUEST);
		} else if (StringUtils.isNumeric(contactNumberId) == false) {
			return new ResponseEntity<String>("Contact number Id is invalid", HttpStatus.BAD_REQUEST);
		}

		ContactNumber phoneNumber = contactNumberDao.findOne(Long.valueOf(contactNumberId));
		if (phoneNumber == null) {
			return new ResponseEntity<String>("Contact number not found", HttpStatus.NOT_FOUND);
		}

		contactNumberDao.delete(phoneNumber);

		return new ResponseEntity<String>("Success - contact number deleted", HttpStatus.OK);
	}
}

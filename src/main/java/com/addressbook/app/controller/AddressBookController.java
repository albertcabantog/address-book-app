package com.addressbook.app.controller;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.addressbook.app.dao.AddressBookDao;
import com.addressbook.app.domain.AddressBook;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class AddressBookController {

	private static final Logger LOG = Logger.getLogger(AddressBookController.class);

	@Autowired
	private AddressBookDao addressBookDao;

	@ApiOperation(value = "Creates new addressbook", response = ResponseEntity.class)
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Success - new address book created"),
			@ApiResponse(code = 400, message = "Address book name is mandatory, Address book already exist") })
	@RequestMapping(value = "/api/createNewAddressBook", method = RequestMethod.PUT)
	public ResponseEntity<String> createNewAddressBook(
			@ApiParam(value = "Name of the new addressbook", required = true) 
			@RequestParam String name) {
		LOG.info("Creating new address book request ");

		if (StringUtils.isBlank(name)) {
			return new ResponseEntity<String>("Address book name is mandatory", HttpStatus.BAD_REQUEST);
		}

		AddressBook addressBook = addressBookDao.findByName(name.trim());
		if (addressBook != null) {
			return new ResponseEntity<String>("Address book already exist", HttpStatus.BAD_REQUEST);
		}

		addressBook = new AddressBook();
		addressBook.setName(name.trim());
		addressBookDao.save(addressBook);

		return new ResponseEntity<String>("Success - new address book created", HttpStatus.OK);
	}

	@ApiOperation(value = "Returns an addressbook using name for search", response = ResponseEntity.class)
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Prints the addressbook found")})
	@RequestMapping(value = "/api/getAddressBookByName", method = RequestMethod.GET)
	public ResponseEntity<AddressBook> getAddressBookByName(
			@ApiParam(value = "Name of the addressbook to search", required = true)
			@RequestParam String name) {

		if (StringUtils.isBlank(name)) {
			return new ResponseEntity<AddressBook>(HttpStatus.BAD_REQUEST);
		}

		AddressBook addressBook = addressBookDao.findByName(name.trim());

		if (addressBook == null) {
			return new ResponseEntity<AddressBook>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<AddressBook>(addressBook, HttpStatus.OK);
	}

	@ApiOperation(value = "Returns all addressbook", response = ResponseEntity.class)
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Prints all the addressbook")})
	@RequestMapping(value = "/api/getAllAddressBook", method = RequestMethod.GET)
	public ResponseEntity<List<AddressBook>> getAllAddressBook() {
		List<AddressBook> addressBooks = addressBookDao.findAll();

		if (CollectionUtils.isEmpty(addressBooks)) {
			return new ResponseEntity<List<AddressBook>>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<List<AddressBook>>(addressBooks, HttpStatus.OK);
	}

	@ApiOperation(value = "Updates the name of the addressbook", response = ResponseEntity.class)
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Success - address book was updated"),
			@ApiResponse(code = 400, message = "No address book selected, Invalid address book id, "
					+ "Address book name is mandatory, Addressbook name already used") })
	@RequestMapping(value = "/api/updateAddressBook", method = RequestMethod.POST)
	public ResponseEntity<String> updateAddressBook(
			@ApiParam(value = "Id of the selected addressbook", required = true)
			@RequestParam String addressBookId, 
			@ApiParam(value = "New name to be used", required = true)
			@RequestParam String newName) {
		if (StringUtils.isBlank(addressBookId)) {
			return new ResponseEntity<String>("No address book selected", HttpStatus.BAD_REQUEST);
		}

		if (StringUtils.isNumeric(addressBookId) == false) {
			return new ResponseEntity<String>("Invalid address book id", HttpStatus.BAD_REQUEST);
		}

		if (StringUtils.isBlank(newName)) {
			return new ResponseEntity<String>("Address book name is mandatory", HttpStatus.BAD_REQUEST);
		}

		AddressBook addressBook = addressBookDao.findByName(newName);
		if (addressBook != null) {
			return new ResponseEntity<String>("Addressbook name already used", HttpStatus.BAD_REQUEST);
		}

		addressBook = addressBookDao.findOne(Long.valueOf(addressBookId));
		addressBook.setName(newName);
		addressBookDao.save(addressBook);

		return new ResponseEntity<String>("Success - address book was updated", HttpStatus.OK);
	}

	@ApiOperation(value = "Deletes the entire addressbook including all contacts and phone numbers", response = ResponseEntity.class)
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Success - address book was deleted"),
			@ApiResponse(code = 400, message = "No address book selected, Invalid address book id, "
					+ "Address book does not exist") })
	@RequestMapping(value = "/api/deleteAddressBook", method = RequestMethod.DELETE)
	public ResponseEntity<String> deleteAddressBook(
			@ApiParam(value = "Id of the selected addressbook", required = true)
			@RequestParam String addressBookId) {
		if (StringUtils.isBlank(addressBookId)) {
			return new ResponseEntity<String>("No address book selected", HttpStatus.BAD_REQUEST);
		}

		if (StringUtils.isNumeric(addressBookId) == false) {
			return new ResponseEntity<String>("Invalid address book id", HttpStatus.BAD_REQUEST);
		}

		AddressBook addressBook = addressBookDao.findOne(Long.valueOf(addressBookId));
		if (addressBook == null) {
			return new ResponseEntity<String>("Address book does not exist", HttpStatus.BAD_REQUEST);
		}

		addressBookDao.delete(addressBook);

		return new ResponseEntity<String>("Success - address book was deleted", HttpStatus.OK);
	}
}

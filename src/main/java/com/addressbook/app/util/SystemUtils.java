package com.addressbook.app.util;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.springframework.util.CollectionUtils;

public final class SystemUtils {

	public static boolean hasDuplicatePhoneNumbers(List<String> numbers) {
		if (CollectionUtils.isEmpty(numbers)) {
			return false;
		}
		
		Set<String> set = new TreeSet<>(numbers);
		
		return numbers.size() != set.size();
	}
}

package com.addressbook.app;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Matchers.any;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.addressbook.app.domain.AddressBook;
import com.addressbook.app.domain.Contact;
import com.addressbook.app.domain.ContactNumber;

public class ContactControllerCreateTest extends BaseControllerTest {

	private static final String PARAM_ADDRESSBOOK_NAME = "addressbookName";
	private static final String PARAM_NEW_ADDRESS_BOOK_FLAG = "newAddressBookFlag";
	private static final String PARAM_CONTACT_NUMBERS = "contactNumbers";
	private static final String PARAM_CONTACT_NAME = "contactName";
	private static final String PARAM_ADDRESS_BOOK_ID = "addressBookId";
	private static final String API_CREATE_NEW_CONTACT = "/api/createNewContact";

	@Test
	public void whenCreatingNewContactWithNewAddressBookButSelectedExistingShouldRaiseBadRequest() throws Exception {
		this.mockMvc
				.perform(put(API_CREATE_NEW_CONTACT).param(PARAM_ADDRESS_BOOK_ID, "1")
						.param(PARAM_CONTACT_NAME, "test contact").param(PARAM_CONTACT_NUMBERS, "123456")
						.param(PARAM_NEW_ADDRESS_BOOK_FLAG, "Y"))
				.andDo(print()).andExpect(status().isBadRequest())
				.andExpect(jsonPath("$").value("Unable to create a new address book for a selected address book"));
	}

	@Test
	public void whenCreatingNewContactWithExistingAddressBookButNotSelectedShouldRaiseBadRequest() throws Exception {
		this.mockMvc
				.perform(put(API_CREATE_NEW_CONTACT).param(PARAM_ADDRESS_BOOK_ID, "")
						.param(PARAM_CONTACT_NAME, "test contact").param(PARAM_CONTACT_NUMBERS, "123456")
						.param(PARAM_NEW_ADDRESS_BOOK_FLAG, ""))
				.andDo(print()).andExpect(status().isBadRequest())
				.andExpect(jsonPath("$").value("Please select an address book"));
	}

	@Test
	public void whenCreatingNewContactWithInvalidAddressBookSelectedShouldRaiseBadRequest() throws Exception {
		this.mockMvc
				.perform(put(API_CREATE_NEW_CONTACT).param(PARAM_ADDRESS_BOOK_ID, "xxx")
						.param(PARAM_CONTACT_NAME, "test contact").param(PARAM_CONTACT_NUMBERS, "123456")
						.param(PARAM_NEW_ADDRESS_BOOK_FLAG, ""))
				.andDo(print()).andExpect(status().isBadRequest())
				.andExpect(jsonPath("$").value("Invalid address book Id"));
	}

	@Test
	public void whenCreatingNewContactWithNewAddressBookButNoNameShouldRaiseBadRequest() throws Exception {
		this.mockMvc
				.perform(put(API_CREATE_NEW_CONTACT).param(PARAM_ADDRESS_BOOK_ID, "")
						.param(PARAM_CONTACT_NAME, "test contact").param(PARAM_CONTACT_NUMBERS, "123456")
						.param(PARAM_NEW_ADDRESS_BOOK_FLAG, "y"))
				.andDo(print()).andExpect(status().isBadRequest())
				.andExpect(jsonPath("$").value("New address book name is required"));
	}
	
	@Test
	public void whenCreatingNewContactWithAnExistingAddressBookAndNewAddressBookNameSpecifiedShouldRaiseBadRequest() throws Exception {

		this.mockMvc
		.perform(put(API_CREATE_NEW_CONTACT).param(PARAM_ADDRESS_BOOK_ID, "111")
				.param(PARAM_CONTACT_NAME, "test contact").param(PARAM_CONTACT_NUMBERS, "123456")
				.param(PARAM_ADDRESSBOOK_NAME, "addressbook name"))
		.andDo(print()).andExpect(status().isBadRequest())
		.andExpect(jsonPath("$").value("Unable to use new addressbook name when using existing addressbook"));
	}

	@Test
	public void whenCreatingNewContactButEmptyContactNameShouldRaiseBadRequest() throws Exception {
		this.mockMvc
				.perform(put(API_CREATE_NEW_CONTACT).param(PARAM_ADDRESS_BOOK_ID, "111").param(PARAM_CONTACT_NAME, "")
						.param(PARAM_CONTACT_NUMBERS, "123456"))
				.andDo(print()).andExpect(status().isBadRequest())
				.andExpect(jsonPath("$").value("Contact name is mandatory"));
	}

	@Test
	public void whenCreatingNewContactButEmptyContactNumberShouldRaiseBadRequest() throws Exception {
		this.mockMvc
				.perform(put(API_CREATE_NEW_CONTACT).param(PARAM_ADDRESS_BOOK_ID, "111")
						.param(PARAM_CONTACT_NAME, "test contact").param(PARAM_CONTACT_NUMBERS, ""))
				.andDo(print()).andExpect(status().isBadRequest())
				.andExpect(jsonPath("$").value("At least one phone number is required"));
	}
	
	@Test
	public void whenCreatingNewContactAndDuplicatePhoneNumberShouldRaiseBadRequest() throws Exception {
		this.mockMvc
				.perform(put(API_CREATE_NEW_CONTACT).param(PARAM_ADDRESS_BOOK_ID, "111")
						.param(PARAM_CONTACT_NAME, "test contact").param(PARAM_CONTACT_NUMBERS, "123, 456,   456 "))
				.andDo(print()).andExpect(status().isBadRequest())
				.andExpect(jsonPath("$").value("Duplicate phone numbers are not allowed"));
	}

	@Test
	public void whenCreatingNewContactWithNewAddressBookButAlreadyExistShouldRaiseBadRequest() throws Exception {
		String addressBookName = "Existing Addressbook";
		AddressBook addressBook = new AddressBook();
		addressBook.setName(addressBookName); 
		given(this.addressBookDao.findByName(addressBookName)).willReturn(addressBook);
		this.mockMvc
				.perform(put(API_CREATE_NEW_CONTACT).param(PARAM_NEW_ADDRESS_BOOK_FLAG, "y")
						.param(PARAM_CONTACT_NAME, "test contact").param(PARAM_CONTACT_NUMBERS, "1234567890")
						.param(PARAM_ADDRESSBOOK_NAME, addressBookName))
				.andDo(print()).andExpect(status().isBadRequest())
				.andExpect(jsonPath("$").value("Address book already exist"));
	}
	
	@Test
	public void whenCreatingNewContactWithExistingAddressBookButNameAlreadyExistShouldRaiseBadRequest() throws Exception {
		String addressBookName = "Existing Addressbook";
		AddressBook addressBook = new AddressBook();
		addressBook.setName(addressBookName); 
		String testContact = "test contact";
		given(this.addressBookDao.findOne(Long.valueOf(111))).willReturn(new AddressBook());
		given(this.contactDao.findByName(new AddressBook(), testContact)).willReturn(new Contact());
		
		this.mockMvc
				.perform(put(API_CREATE_NEW_CONTACT).param(PARAM_ADDRESS_BOOK_ID, "111").param(PARAM_NEW_ADDRESS_BOOK_FLAG, "n")
						.param(PARAM_CONTACT_NAME, testContact).param(PARAM_CONTACT_NUMBERS, "1234567890")
						.param(PARAM_ADDRESSBOOK_NAME, ""))
				.andDo(print()).andExpect(status().isBadRequest())
				.andExpect(jsonPath("$").value("Name already exist"));
	}

	@Test
	public void whenCreatingNewContactWithNewAddressBookSuccess() throws Exception {
		String addressBookName = "Addressbook";
		AddressBook addressBook = new AddressBook();
		addressBook.setName(addressBookName);

		String contactName = "test contact";
		Contact contact = new Contact();
		contact.setAddressBook(addressBook);
		contact.setName(contactName);

		String contactNumbers = "1234567890";
		ContactNumber contactNumber = new ContactNumber();
		contactNumber.setPhoneNumber(contactNumbers);
		List<ContactNumber> numbers = new ArrayList<>();
		numbers.add(contactNumber);

		given(this.addressBookDao.findByName(addressBookName)).willReturn(null);
		given(this.contactDao.save(contact)).willReturn(contact);

		this.mockMvc
				.perform(put(API_CREATE_NEW_CONTACT).param(PARAM_NEW_ADDRESS_BOOK_FLAG, "y")
						.param(PARAM_CONTACT_NAME, contactName).param(PARAM_CONTACT_NUMBERS, contactNumbers)
						.param(PARAM_ADDRESSBOOK_NAME, addressBookName))
				.andDo(print()).andExpect(status().isOk())
				.andExpect(jsonPath("$").value("Success - new contact created"));

		verify(addressBookDao, times(1)).save(addressBook);
		verify(contactDao, times(2)).save(any(Contact.class));
	}

	@Test
	public void whenCreatingNewContactWithExistingAddressBookSuccess() throws Exception {
		String addressBookName = "Addressbook";
		AddressBook addressBook = new AddressBook();
		addressBook.setId(111);
		addressBook.setName(addressBookName);

		String contactName = "test contact";
		Contact contact = new Contact();
		contact.setAddressBook(addressBook);
		contact.setName(contactName);

		String contactNumbers = "1234567890";
		ContactNumber contactNumber = new ContactNumber();
		contactNumber.setPhoneNumber(contactNumbers);
		List<ContactNumber> numbers = new ArrayList<>();
		numbers.add(contactNumber);

		given(this.addressBookDao.findOne(Long.valueOf(111))).willReturn(addressBook);
		given(this.contactDao.save(contact)).willReturn(new Contact());

		this.mockMvc
				.perform(put(API_CREATE_NEW_CONTACT).param(PARAM_ADDRESS_BOOK_ID, "111")
						.param(PARAM_CONTACT_NAME, contactName).param(PARAM_CONTACT_NUMBERS, contactNumbers))
				.andDo(print()).andExpect(status().isOk())
				.andExpect(jsonPath("$").value("Success - new contact created"));

		verify(contactDao, times(2)).save(any(Contact.class));
	}

}

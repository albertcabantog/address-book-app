package com.addressbook.app;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.AnnotationConfigWebContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.addressbook.app.dao.AddressBookDao;
import com.addressbook.app.dao.ContactDao;
import com.addressbook.app.dao.ContactNumberDao;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = AnnotationConfigWebContextLoader.class)
@WebAppConfiguration
public abstract class BaseControllerTest {
	
	@Configuration
	@EnableWebMvc
	@ComponentScan(basePackages = { "com.addressbook.app.controller" })
	static class ContextConfiguration {
	}

	@Autowired
	protected WebApplicationContext wac;

	protected MockMvc mockMvc;

	@MockBean
	protected AddressBookDao addressBookDao;
	
	@MockBean
	protected ContactDao contactDao;
	
	@MockBean
	protected ContactNumberDao contactNumberDao;

	@Before
	public void setup() throws Exception {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
	}

}

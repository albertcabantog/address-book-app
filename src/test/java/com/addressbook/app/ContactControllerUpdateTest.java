package com.addressbook.app;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;

import com.addressbook.app.domain.AddressBook;
import com.addressbook.app.domain.Contact;
import com.addressbook.app.domain.ContactNumber;

public class ContactControllerUpdateTest extends BaseControllerTest {

	private static final String API_ADD_CONTACT_NUMBER = "/api/addContactNumber";
	private static final String PARAM_NEW_CONTACT_NUMBER = "newContactNumber";
	private static final String PARAM_CONTACT_NUMBER_ID = "contactNumberId";
	private static final String API_UPDATE_CONTACT_NUMBER = "/api/updateContactNumber";
	private static final String PARAM_CONTACT_NAME = "contactName";
	private static final String PARAM_CONTACT_ID = "contactId";
	private static final String API_UPDATE_CONTACT_NAME = "/api/updateContactName";

	@Test
	public void whenUpdatingContactNameWithNoContactSelectedShouldRaiseBadRequest() throws Exception {
		this.mockMvc
				.perform(
						post(API_UPDATE_CONTACT_NAME).param(PARAM_CONTACT_ID, "").param(PARAM_CONTACT_NAME, "new name"))
				.andDo(print()).andExpect(status().isBadRequest())
				.andExpect(jsonPath("$").value("Contact not selected"));
	}

	@Test
	public void whenUpdatingContactNameWithInvalidContactIdShouldRaiseBadRequest() throws Exception {
		this.mockMvc
				.perform(post(API_UPDATE_CONTACT_NAME).param(PARAM_CONTACT_ID, "1.1").param(PARAM_CONTACT_NAME,
						"new name"))
				.andDo(print()).andExpect(status().isBadRequest())
				.andExpect(jsonPath("$").value("Contact Id is invalid"));
	}

	@Test
	public void whenUpdatingContactNameWithNewNameEmptyRaiseBadRequest() throws Exception {
		this.mockMvc.perform(post(API_UPDATE_CONTACT_NAME).param(PARAM_CONTACT_ID, "1").param(PARAM_CONTACT_NAME, ""))
				.andDo(print()).andExpect(status().isBadRequest())
				.andExpect(jsonPath("$").value("New name is required"));
	}

	@Test
	public void whenUpdatingContactNameButContactDoesNotExistShouldRaiseNotFound() throws Exception {
		given(this.contactDao.findOne(Long.valueOf(1))).willReturn(null);
		this.mockMvc
				.perform(post(API_UPDATE_CONTACT_NAME).param(PARAM_CONTACT_ID, "1").param(PARAM_CONTACT_NAME,
						"new name"))
				.andDo(print()).andExpect(status().isNotFound()).andExpect(jsonPath("$").value("Contact not found"));
	}

	@Test
	public void whenUpdatingContactNameButNameAlreadyUsedThenRaiseBadRequest() throws Exception {
		Contact testContact = new Contact();
		AddressBook testAddressBook = new AddressBook();
		testContact.setAddressBook(testAddressBook);
		given(this.contactDao.findOne(Long.valueOf(1))).willReturn(testContact);
		given(this.contactDao.findByName(testAddressBook, "new name")).willReturn(testContact);
		this.mockMvc
				.perform(post(API_UPDATE_CONTACT_NAME).param(PARAM_CONTACT_ID, "1").param(PARAM_CONTACT_NAME,
						"new name"))
				.andDo(print()).andExpect(status().isBadRequest()).andExpect(jsonPath("$").value("Contact name already used"));
	}

	@Test
	public void whenUpdatingContactNameAllValidThenSuccess() throws Exception {
		given(this.contactDao.findOne(Long.valueOf(1))).willReturn(new Contact());
		this.mockMvc
				.perform(post(API_UPDATE_CONTACT_NAME).param(PARAM_CONTACT_ID, "1").param(PARAM_CONTACT_NAME,
						"new name"))
				.andDo(print()).andExpect(status().isOk())
				.andExpect(jsonPath("$").value("Success - contact name updated"));

		verify(contactDao, times(1)).save(any(Contact.class));
	}

	@Test
	public void whenAddingNewContactNumberButNoContactSelectedThenRaiseBadRequest() throws Exception {
		this.mockMvc
				.perform(put(API_ADD_CONTACT_NUMBER).param(PARAM_CONTACT_ID, "").param(PARAM_NEW_CONTACT_NUMBER, ""))
				.andDo(print()).andExpect(status().isBadRequest())
				.andExpect(jsonPath("$").value("Contact not selected"));
	}

	@Test
	public void whenAddingNewContactNumberButContactIdInvalidThenRaiseBadRequest() throws Exception {
		this.mockMvc
				.perform(put(API_ADD_CONTACT_NUMBER).param(PARAM_CONTACT_ID, "xxx").param(PARAM_NEW_CONTACT_NUMBER, ""))
				.andDo(print()).andExpect(status().isBadRequest())
				.andExpect(jsonPath("$").value("Contact Id is invalid"));
	}

	@Test
	public void whenAddingNewContactNumberButEmptyThenRaiseBadRequest() throws Exception {
		this.mockMvc
				.perform(put(API_ADD_CONTACT_NUMBER).param(PARAM_CONTACT_ID, "111").param(PARAM_NEW_CONTACT_NUMBER, ""))
				.andDo(print()).andExpect(status().isBadRequest())
				.andExpect(jsonPath("$").value("New contact number is required"));
	}

	@Test
	public void whenAddingNewContactNumberButContactDoesNotExistThenRaiseNotFound() throws Exception {
		given(this.contactDao.findOne(Long.valueOf(1))).willReturn(null);
		this.mockMvc
				.perform(put(API_ADD_CONTACT_NUMBER).param(PARAM_CONTACT_ID, "111").param(PARAM_NEW_CONTACT_NUMBER,
						"123"))
				.andDo(print()).andExpect(status().isNotFound()).andExpect(jsonPath("$").value("Contact not found"));
	}

	@Test
	public void whenAddingNewContactNumberButNumberAlreadyUsedThenRaiseBadRequest() throws Exception {
		given(this.contactDao.findOne(Long.valueOf(111))).willReturn(new Contact());
		given(this.contactNumberDao.findByPhoneNumber(new Contact(), "123")).willReturn(new ContactNumber());
		this.mockMvc
				.perform(put(API_ADD_CONTACT_NUMBER).param(PARAM_CONTACT_ID, "111").param(PARAM_NEW_CONTACT_NUMBER,
						"123"))
				.andDo(print()).andExpect(status().isBadRequest())
				.andExpect(jsonPath("$").value("Phone number already used"));
	}

	@Test
	public void whenAddingNewContactNumberSuccess() throws Exception {
		given(this.contactDao.findOne(Long.valueOf(111))).willReturn(new Contact());
		given(this.contactNumberDao.findByPhoneNumber(new Contact(), "123")).willReturn(null);
		this.mockMvc
				.perform(put(API_ADD_CONTACT_NUMBER).param(PARAM_CONTACT_ID, "111").param(PARAM_NEW_CONTACT_NUMBER,
						"123"))
				.andDo(print()).andExpect(status().isOk())
				.andExpect(jsonPath("$").value("Success - contact number added"));

		verify(contactNumberDao, times(1)).save(any(ContactNumber.class));
	}

	@Test
	public void whenUpdatingContactNumberWithNoContactNumberSelectedShouldRaiseBadRequest() throws Exception {
		this.mockMvc
				.perform(post(API_UPDATE_CONTACT_NUMBER).param(PARAM_CONTACT_NUMBER_ID, "")
						.param(PARAM_NEW_CONTACT_NUMBER, ""))
				.andDo(print()).andExpect(status().isBadRequest())
				.andExpect(jsonPath("$").value("Contact number not selected"));
	}

	@Test
	public void whenUpdatingContactNumberWithInvalidContactNumberIdShouldRaiseBadRequest() throws Exception {
		this.mockMvc
				.perform(post(API_UPDATE_CONTACT_NUMBER).param(PARAM_CONTACT_NUMBER_ID, "a ")
						.param(PARAM_NEW_CONTACT_NUMBER, ""))
				.andDo(print()).andExpect(status().isBadRequest())
				.andExpect(jsonPath("$").value("Contact number Id is invalid"));
	}

	@Test
	public void whenUpdatingContactNumberWithNewContactNumberEmptyShouldRaiseBadRequest() throws Exception {
		this.mockMvc
				.perform(post(API_UPDATE_CONTACT_NUMBER).param(PARAM_CONTACT_NUMBER_ID, "100")
						.param(PARAM_NEW_CONTACT_NUMBER, ""))
				.andDo(print()).andExpect(status().isBadRequest())
				.andExpect(jsonPath("$").value("New contact number is required"));
	}

	@Test
	public void whenUpdatingContactNumberButContactNumberDoesNotExistShouldRaiseNotFound() throws Exception {
		given(this.contactNumberDao.findOne(Long.valueOf(1))).willReturn(null);
		this.mockMvc
				.perform(post(API_UPDATE_CONTACT_NUMBER).param(PARAM_CONTACT_NUMBER_ID, "1")
						.param(PARAM_NEW_CONTACT_NUMBER, "123456"))
				.andDo(print()).andExpect(status().isNotFound())
				.andExpect(jsonPath("$").value("Contact number not found"));
	}
	
	@Test
	public void whenUpdatingContactNumberButContactNumberAlreadyUsedShouldRaiseBadRequest() throws Exception {
		ContactNumber contactNumber = new ContactNumber();
		Contact contact = new Contact();
		contactNumber.setContact(contact);
		given(this.contactNumberDao.findOne(Long.valueOf(1))).willReturn(contactNumber);
		given(this.contactNumberDao.findByPhoneNumber(contact, "123456")).willReturn(contactNumber);
		this.mockMvc
				.perform(post(API_UPDATE_CONTACT_NUMBER).param(PARAM_CONTACT_NUMBER_ID, "1")
						.param(PARAM_NEW_CONTACT_NUMBER, "123456"))
				.andDo(print()).andExpect(status().isBadRequest())
				.andExpect(jsonPath("$").value("Phone number already used"));
	}

	@Test
	public void whenUpdatingContactNumberAllValidThenSuccess() throws Exception {
		given(this.contactNumberDao.findOne(Long.valueOf(1))).willReturn(new ContactNumber());
		this.mockMvc
				.perform(post(API_UPDATE_CONTACT_NUMBER).param(PARAM_CONTACT_NUMBER_ID, "1")
						.param(PARAM_NEW_CONTACT_NUMBER, "123456"))
				.andDo(print()).andExpect(status().isOk())
				.andExpect(jsonPath("$").value("Success - contact number updated"));

		verify(contactNumberDao, times(1)).save(any(ContactNumber.class));
	}
}

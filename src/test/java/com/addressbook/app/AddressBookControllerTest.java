package com.addressbook.app;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.addressbook.app.domain.AddressBook;

public class AddressBookControllerTest extends BaseControllerTest {

	private static final String API_DELETE_ADDRESS_BOOK = "/api/deleteAddressBook";

	private static final String PARAM_NEW_NAME = "newName";

	private static final String PARAM_ADDRESS_BOOK_ID = "addressBookId";

	private static final String API_UPDATE_ADDRESS_BOOK = "/api/updateAddressBook";

	private static final String API_GET_ADDRESS_BOOK_BY_NAME = "/api/getAddressBookByName";

	private static final String API_GET_ALL_ADDRESS_BOOK = "/api/getAllAddressBook";

	private static final String PARAM_NAME = "name";

	private static final String API_CREATE_NEW_ADDRESS_BOOK = "/api/createNewAddressBook";

	@Test
	public void whenCreatingNewAddressBookCheckForName() throws Exception {
		this.mockMvc.perform(put(API_CREATE_NEW_ADDRESS_BOOK).param(PARAM_NAME, "")).andDo(print())
				.andExpect(status().isBadRequest()).andExpect(jsonPath("$").value("Address book name is mandatory"));
	}

	@Test
	public void whenCreatingNewAddressBookCheckForDuplicateName() throws Exception {
		String duplicateName = "AnotherAddressBookExist";
		AddressBook test = new AddressBook();
		test.setName(duplicateName);

		given(this.addressBookDao.findByName(duplicateName)).willReturn(test);

		this.mockMvc.perform(put(API_CREATE_NEW_ADDRESS_BOOK).param(PARAM_NAME, duplicateName)).andDo(print())
				.andExpect(status().isBadRequest()).andExpect(jsonPath("$").value("Address book already exist"));
	}

	@Test
	public void whenCreatingNewAddressBookCheckForSuccessMessage() throws Exception {
		String addressBookName = "AddressBook";

		given(this.addressBookDao.findByName(addressBookName)).willReturn(null);

		this.mockMvc.perform(put(API_CREATE_NEW_ADDRESS_BOOK).param(PARAM_NAME, addressBookName)).andDo(print())
				.andExpect(status().isOk()).andExpect(jsonPath("$").value("Success - new address book created"));
	}

	@Test
	public void whenGettingAddressBookWithEmptyNameShouldRaiseBadRequest() throws Exception {
		this.mockMvc.perform(get(API_GET_ADDRESS_BOOK_BY_NAME).param(PARAM_NAME, "")).andDo(print())
				.andExpect(status().isBadRequest());
	}

	@Test
	public void whenGettingAddressBookByNameThatDoesNotExistShouldRaiseNotFound() throws Exception {
		String addressBookName = "AddressBook";

		given(this.addressBookDao.findByName(addressBookName)).willReturn(null);

		this.mockMvc.perform(get(API_GET_ADDRESS_BOOK_BY_NAME).param(PARAM_NAME, addressBookName)).andDo(print())
				.andExpect(status().isNotFound());
	}

	@Test
	public void whenGettingAddressBookByNameThenFoundSuccess() throws Exception {
		String addressBookName = "AddressBook";

		given(this.addressBookDao.findByName(addressBookName))
				.willReturn(new AddressBook());

		this.mockMvc.perform(get(API_GET_ADDRESS_BOOK_BY_NAME).param(PARAM_NAME, addressBookName)).andDo(print())
				.andExpect(status().isOk()).andExpect(jsonPath("$").isNotEmpty());
	}

	@Test
	public void whenGettingAllAddressBookDoesNotExistThenRaiseNotFound() throws Exception {

		given(this.addressBookDao.findAll()).willReturn(null);

		this.mockMvc.perform(get(API_GET_ALL_ADDRESS_BOOK)).andDo(print()).andExpect(status().isNotFound());
	}

	@Test
	public void whenGettingAllAddressBookFoundSuccess() throws Exception {

		List<AddressBook> addressBooks = new ArrayList<>();
		addressBooks.add(new AddressBook());
		given(this.addressBookDao.findAll()).willReturn(addressBooks);

		this.mockMvc.perform(get(API_GET_ALL_ADDRESS_BOOK)).andDo(print()).andExpect(status().isOk())
				.andExpect(jsonPath("$").isArray());
	}

	@Test
	public void whenUpdatingAddressBookButNotSelectedIdRaiseBadRequest() throws Exception {

		this.mockMvc
				.perform(post(API_UPDATE_ADDRESS_BOOK).param(PARAM_ADDRESS_BOOK_ID, "").param(PARAM_NEW_NAME, "test"))
				.andDo(print()).andExpect(status().isBadRequest())
				.andExpect(jsonPath("$").value("No address book selected"));
	}

	@Test
	public void whenUpdatingAddressBookButIdIsInvalid() throws Exception {

		this.mockMvc
				.perform(
						post(API_UPDATE_ADDRESS_BOOK).param(PARAM_ADDRESS_BOOK_ID, "xxx").param(PARAM_NEW_NAME, "test"))
				.andDo(print()).andExpect(status().isBadRequest())
				.andExpect(jsonPath("$").value("Invalid address book id"));
	}

	@Test
	public void whenUpdatingAddressBookNewNameIsEmptyThenRaiseBadRequest() throws Exception {

		this.mockMvc
				.perform(post(API_UPDATE_ADDRESS_BOOK).param(PARAM_ADDRESS_BOOK_ID, "100").param(PARAM_NEW_NAME, ""))
				.andDo(print()).andExpect(status().isBadRequest())
				.andExpect(jsonPath("$").value("Address book name is mandatory"));
	}

	@Test
	public void whenUpdatingAddressBookSameWithTheOldNameIsInvalid() throws Exception {
		String oldName = "AddressBook";
		String newName = "AddressBook";
		AddressBook addressBook = new AddressBook();
		addressBook.setId(1);
		addressBook.setName(oldName);
		given(this.addressBookDao.findByName(oldName)).willReturn(addressBook);

		this.mockMvc
				.perform(post(API_UPDATE_ADDRESS_BOOK).param(PARAM_ADDRESS_BOOK_ID, "1").param(PARAM_NEW_NAME, newName))
				.andDo(print()).andExpect(status().isBadRequest())
				.andExpect(jsonPath("$").value("Addressbook name already used"));
	}

	@Test
	public void whenUpdatingAddressBookNewNameSuccess() throws Exception {
		String oldName = "AddressBook";
		String newName = "NewAddressBOOK";
		AddressBook addressBook = new AddressBook();
		addressBook.setId(1);
		addressBook.setName(oldName);
		given(this.addressBookDao.findByName(oldName)).willReturn(null);
		given(this.addressBookDao.findOne(Long.valueOf(1))).willReturn(addressBook);

		this.mockMvc
				.perform(post(API_UPDATE_ADDRESS_BOOK).param(PARAM_ADDRESS_BOOK_ID, "1").param(PARAM_NEW_NAME, newName))
				.andDo(print()).andExpect(status().isOk())
				.andExpect(jsonPath("$").value("Success - address book was updated"));
	}

	@Test
	public void whenDeletingAddressBookButNotSelectedIdShouldRaiseBadRequest() throws Exception {
		this.mockMvc.perform(delete(API_DELETE_ADDRESS_BOOK).param(PARAM_ADDRESS_BOOK_ID, "")).andDo(print())
				.andExpect(status().isBadRequest()).andExpect(jsonPath("$").value("No address book selected"));
	}

	@Test
	public void whenDeletingAddressBookButIdIsInvalid() throws Exception {

		this.mockMvc.perform(delete(API_DELETE_ADDRESS_BOOK).param(PARAM_ADDRESS_BOOK_ID, "invalid")).andDo(print())
				.andExpect(status().isBadRequest()).andExpect(jsonPath("$").value("Invalid address book id"));
	}

	@Test
	public void whenDeletingAddressBookButDoesNotExistAnymoreThenRaiseBadRequest() throws Exception {

		AddressBook addressBook = new AddressBook();
		addressBook.setId(123);
		addressBook.setName("TestAddressBook");
		given(this.addressBookDao.findOne(Long.valueOf(123))).willReturn(null);

		this.mockMvc.perform(delete(API_DELETE_ADDRESS_BOOK).param(PARAM_ADDRESS_BOOK_ID, "123")).andDo(print())
				.andExpect(status().isBadRequest()).andExpect(jsonPath("$").value("Address book does not exist"));
	}

	@Test
	public void whenDeletingAddressBookSuccess() throws Exception {

		AddressBook addressBook = new AddressBook();
		addressBook.setId(123);
		addressBook.setName("TestAddressBook");
		given(this.addressBookDao.findOne(Long.valueOf(123))).willReturn(addressBook);

		this.mockMvc.perform(delete(API_DELETE_ADDRESS_BOOK).param(PARAM_ADDRESS_BOOK_ID, "123")).andDo(print())
				.andExpect(status().isOk()).andExpect(jsonPath("$").value("Success - address book was deleted"));
		
		verify(addressBookDao, times(1)).delete(addressBook);
	}
}

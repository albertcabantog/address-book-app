package com.addressbook.app;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.addressbook.app.domain.Contact;
import com.addressbook.app.domain.UniqueContact;

public class ContactControllerGetTest extends BaseControllerTest {

	private static final String API_GET_DISTINCT_CONTACTS = "/api/getDistinctContacts";
	private static final String HEADER_ERROR_MESSAGE = "error-message";
	private static final String PARAM_CONTACT_BOOK_ID = "contactId";
	private static final String API_GET_CONTACTS = "/api/getContact";

	@Test
	public void whenSearchingContactsNoAddressBookSelectedShouldRaiseBadRequest() throws Exception {
		this.mockMvc.perform(get(API_GET_CONTACTS).param(PARAM_CONTACT_BOOK_ID, "")).andDo(print())
				.andExpect(status().isBadRequest())
				.andExpect(header().string(HEADER_ERROR_MESSAGE, "Contact not selected"));
	}

	@Test
	public void whenSearchingContactsWithInvalidAddressBookShouldRaiseBadRequest() throws Exception {
		this.mockMvc.perform(get(API_GET_CONTACTS).param(PARAM_CONTACT_BOOK_ID, "xxx")).andDo(print())
				.andExpect(status().isBadRequest())
				.andExpect(header().string(HEADER_ERROR_MESSAGE, "Contact Id is invalid"));
	}

	@Test
	public void whenSearchingContactsAddressBookDoesNotExistShouldRaiseNotFound() throws Exception {
		given(this.contactDao.findOne(Long.valueOf(100))).willReturn(null);

		this.mockMvc.perform(get(API_GET_CONTACTS).param(PARAM_CONTACT_BOOK_ID, "100")).andDo(print())
				.andExpect(status().isNotFound()).andExpect(header().string(HEADER_ERROR_MESSAGE, "Contact not found"));
	}

	@Test
	public void whenSearchingContactSuccess() throws Exception {
		given(this.contactDao.findOne(Long.valueOf(111))).willReturn(new Contact());

		this.mockMvc.perform(get(API_GET_CONTACTS).param(PARAM_CONTACT_BOOK_ID, "111")).andDo(print())
				.andExpect(status().isOk()).andExpect(jsonPath("$").isNotEmpty());

	}
	
	@Test
	public void whenSearchingWithNoDistinctContactsThenRaiseNotFound() throws Exception {
		given(this.contactDao.getDistinctContacts()).willReturn(null);

		this.mockMvc.perform(get(API_GET_DISTINCT_CONTACTS)).andDo(print())
				.andExpect(status().isNotFound()).andExpect(header().string(HEADER_ERROR_MESSAGE, "Contact not found"));

	}
	
	@Test
	public void whenSearchingWithDistinctContactsExistThenSuccess() throws Exception {
		List<UniqueContact> uniqueContacts = new ArrayList<>();
		uniqueContacts.add(new UniqueContact());
		given(this.contactDao.getDistinctContacts()).willReturn(uniqueContacts);

		this.mockMvc.perform(get(API_GET_DISTINCT_CONTACTS)).andDo(print())
				.andExpect(status().isOk()).andExpect(jsonPath("$").isNotEmpty())
				.andExpect(jsonPath("$").isArray());

	}

}

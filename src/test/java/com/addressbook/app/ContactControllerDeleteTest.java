package com.addressbook.app;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;

import com.addressbook.app.domain.Contact;
import com.addressbook.app.domain.ContactNumber;

public class ContactControllerDeleteTest extends BaseControllerTest {

	private static final String PARAM_CONTACT_NUMBER_ID = "contactNumberId";
	private static final String API_DELETE_CONTACT_NUMBER = "/api/deleteContactNumber";
	private static final String PARAM_CONTACT_ID = "contactId";
	private static final String API_DELETE_CONTACT_NAME = "/api/deleteContact";

	@Test
	public void whenDeletingContactWithNoContactSelectedShouldRaiseBadRequest() throws Exception {
		this.mockMvc.perform(delete(API_DELETE_CONTACT_NAME).param(PARAM_CONTACT_ID, "")).andDo(print())
				.andExpect(status().isBadRequest()).andExpect(jsonPath("$").value("Contact not selected"));
	}

	@Test
	public void whenDeletingContactWithInvalidContactIdShouldRaiseBadRequest() throws Exception {
		this.mockMvc.perform(delete(API_DELETE_CONTACT_NAME).param(PARAM_CONTACT_ID, "1.1")).andDo(print())
				.andExpect(status().isBadRequest()).andExpect(jsonPath("$").value("Contact Id is invalid"));
	}

	@Test
	public void whenDeletingContactButContactDoesNotExistShouldRaiseNotFound() throws Exception {
		given(this.contactDao.findOne(Long.valueOf(1))).willReturn(null);
		this.mockMvc.perform(delete(API_DELETE_CONTACT_NAME).param(PARAM_CONTACT_ID, "1")).andDo(print())
				.andExpect(status().isNotFound()).andExpect(jsonPath("$").value("Contact not found"));
	}

	@Test
	public void whenDeletingContactAllValidThenSuccess() throws Exception {
		given(this.contactDao.findOne(Long.valueOf(1))).willReturn(new Contact());
		this.mockMvc.perform(delete(API_DELETE_CONTACT_NAME).param(PARAM_CONTACT_ID, "1")).andDo(print())
				.andExpect(status().isOk()).andExpect(jsonPath("$").value("Success - contact deleted"));

		verify(contactDao, times(1)).delete(any(Contact.class));
	}

	@Test
	public void whenDeletingContactNumberWithNoContactNumberSelectedShouldRaiseBadRequest() throws Exception {
		this.mockMvc.perform(delete(API_DELETE_CONTACT_NUMBER).param(PARAM_CONTACT_NUMBER_ID, "")).andDo(print())
				.andExpect(status().isBadRequest()).andExpect(jsonPath("$").value("Contact number not selected"));
	}

	@Test
	public void whenDeletingContactNumberWithInvalidContactNumberIdShouldRaiseBadRequest() throws Exception {
		this.mockMvc.perform(delete(API_DELETE_CONTACT_NUMBER).param(PARAM_CONTACT_NUMBER_ID, "A1")).andDo(print())
				.andExpect(status().isBadRequest()).andExpect(jsonPath("$").value("Contact number Id is invalid"));
	}

	@Test
	public void whenDeletingContactNumberButContactNumberDoesNotExistShouldRaiseNotFound() throws Exception {
		given(this.contactNumberDao.findOne(Long.valueOf(1))).willReturn(null);
		this.mockMvc.perform(delete(API_DELETE_CONTACT_NUMBER).param(PARAM_CONTACT_NUMBER_ID, "1")).andDo(print())
				.andExpect(status().isNotFound()).andExpect(jsonPath("$").value("Contact number not found"));
	}

	@Test
	public void whenDeletingContactNumberAllValidThenSuccess() throws Exception {
		given(this.contactNumberDao.findOne(Long.valueOf(1))).willReturn(new ContactNumber());
		this.mockMvc.perform(delete(API_DELETE_CONTACT_NUMBER).param(PARAM_CONTACT_NUMBER_ID, "1")).andDo(print())
				.andExpect(status().isOk()).andExpect(jsonPath("$").value("Success - contact number deleted"));

		verify(contactNumberDao, times(1)).delete(any(ContactNumber.class));
	}
}
